<!DOCTYPE html>
<html>
<head>
<style>
input[type=text], input[type=password],textarea,input[type=date] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

.cancelbtn {
    padding: 14px 20px;
    background-color: #f44336;
}

.cancelbtn,.signupbtn {float:left;width:50%}


.container {
    padding: 16px;
}


.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}
</style>
</head>
<body onload="document.getElementById('id01').style.display='block'" background="images/b1.jpg">
<header style="border: double;background-image: url(b1.jpg);color: white;">
    <CENTER>
    <h1> SEAWORLD AQUATICA WATERPARK AND RESORT</h1>
    </CENTER>
  </header>
<div id="id01" class="modal">
  
  <form class="modal-content" action="reguser.php" method="post" onsubmit="return check()" name="frm1">
    <div class="container">
      <h1>Register</h1>
      <label><b>Full Name</b></label>
      <input type="text" placeholder="Enter FullName" name="fullname" required>
      
      <label><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="user" required>
      
      <label><b>Email</b></label>
      <input type="text" placeholder="Enter Email" name="email" required>

      <label><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="psw" required>

      <label><b>Confirm Password</b></label>
      <input type="password" placeholder="Confirm Password" name="pswrepeat" required>

      <label><b>Mobile Number</b></label>
      <input type="text" placeholder="Enter Mobile no" name="mob" required>

      <label><b>Address</b></label>
      <textarea placeholder="Enter Address" name="addr" rows="5" cols="20"></textarea>

      <label><b>Date Of Birth</b></label>
      <input type="Date" placeholder="DOB" name="dob">


      <input type="checkbox" checked="checked"> Remember me
      <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

      
        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        <button type="submit" class="signupbtn">Sign Up</button>
      
    </div>
  </form>
</div>
<script language="javascript">
  function check() 
  {
    var x=document.frm1.email.value;
    var y=document.frm1.psw.value;
    var z=document.frm1.pswrepeat.value;
    a=x.lastIndexOf('@');
    b=x.lastIndexOf('.');
    c=x.length;
    if(a>b || a==-1 || b==-1 || a==c || b==c)
    {
      alert("Email id not Valid");
      return false;
    }
    if(y!=z)
    {
      alert("Passwords do not Match!");
      return false;
    }

  }
</script>


</body>
</html>
